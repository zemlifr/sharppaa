﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SharpPAA
{
    class GeneticAlgorithm : Algorithm
    {
        protected double mutationProb = 0.05;
        protected int popSize= 100;
        protected int maxCycles = 100;

        public GeneticAlgorithm(KnapsackProblem problem) : base(problem)
        {
        }

        public override Result Solve()
        {
            List<Individum> pop = InitPopulation(popSize);
            pop.Sort(new FitnessComparator());
            
            Random rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < maxCycles; i++)
            {
                List<Individum> newPop = new List<Individum>();

                for (int j = 0; j < popSize/2; j++)
                {
                    Individum a = pop[0];
                    pop.RemoveAt(0);
                    Individum b = pop[0];
                    pop.RemoveAt(0);

                    var breed = Crossover(a, b);

                    newPop.Add(a);
                    newPop.Add(b);
                    newPop.Add(breed.Item1);
                    newPop.Add(breed.Item2);
                }

                foreach (var ind in newPop)
                {
                    if (rand.NextDouble() < mutationProb)
                        Mutate(ind);
                }

                newPop.Sort(new FitnessComparator());

                
                pop = newPop;
            }
            Individum u = pop[0];
            int wsum = 0;
            int psum = 0;

            for (int i = 0; i < u.Data.Length; i++)
            {
                psum += Convert.ToInt32(u.Data[i]) * problem.Items[i].price;
                wsum += Convert.ToInt32(u.Data[i]) * problem.Items[i].weight;
            }

            return new Result(psum,wsum); 
        }

        protected Tuple<Individum,Individum> Crossover(Individum a, Individum b)
        {
            Individum u = new Individum(problem.Items.Length);
            Individum v = new Individum(problem.Items.Length);

            Random rand = new Random(DateTime.Now.Millisecond);
            var idx = rand.Next(1, a.Data.Length-1);
            Array.Copy(a.Data,0,u.Data,0,idx);
            Array.Copy(b.Data, 0, v.Data, 0, idx);

            Array.Copy(a.Data, idx, v.Data, idx, a.Data.Length-idx);
            Array.Copy(b.Data, idx, u.Data, idx, b.Data.Length-idx);

            u.ComputeFitness(problem);
            v.ComputeFitness(problem);

            return new Tuple<Individum, Individum>(u, v);
        }

        protected void Mutate(Individum u)
        {
            Random rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < u.Data.Length; i++)
                if(rand.NextDouble() < 1.0/u.Data.Length)
                    u.Data[i] = !u.Data[i];
            u.ComputeFitness(problem);
        }

        private List<Individum> InitPopulation(int size)
        {
            List<Individum> pop = new List<Individum>();
            Random rand = new Random(DateTime.Now.Millisecond);

            for(int i = 0; i<size;i++)
            {
                Individum ind = new Individum(problem.Items.Length);
                var data = new bool[problem.Items.Length];
                for (int j = 0; j < data.Length; j++)
                {
                    var x = rand.NextDouble();
                    data[j] = x >= 0.5;
                }
                ind.Data = data;
                ind.ComputeFitness(problem);
                pop.Add(ind);
            }

            return pop;
        }

        protected class Individum
        {
            private bool[] data;
            private int fitness = 0;

            public Individum(int size)
            {
                data = new bool[size];
            }

            public bool[] Data
            {
                get { return data; }
                set { data = value; }
            }

            public void ComputeFitness(KnapsackProblem prob)
            {
                int wsum = 0;
                int psum = 0;

                for (int i = 0; i < data.Length; i++)
                {
                    psum += Convert.ToInt32(data[i])*prob.Items[i].price;
                    wsum += Convert.ToInt32(data[i]) * prob.Items[i].weight;
                }

                fitness = psum;
                fitness *= wsum/4;

                if (wsum > prob.MaxWeight)
                    fitness = -1;
            }

            public int GetFitness()
            {
                return fitness;
            }
        }

        class FitnessComparator : IComparer<Individum>
        {
            public int Compare(Individum x, Individum y)
            {
                var a = x.GetFitness();
                var b = y.GetFitness();
                return -a.CompareTo(b);
            }
        }
    }
}
