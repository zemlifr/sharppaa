﻿using System;
using System.Linq;

namespace SharpPAA
{
    class FptasAlgorithm : Algorithm
    {
        public FptasAlgorithm(KnapsackProblem problem, float eps) : base(problem)
        {
            epsilon = eps;
        }

        public override Result Solve()
        {
            Init();
            Item[] items = new Item[trimmed.Items.Length + 1];
            items[0] = new Item(0, 0);
            Array.Copy(trimmed.Items, 0, items, 1, trimmed.Items.Length);

            for (int i = 0; i < problem.Items.Length; i++)
            {
                for (int c = 0; c <= maxPrice; c++)
                {
                    int w;
                    Item it = items[i + 1];
                    if (c - it.price < 0 || weights[i, c - it.price] == int.MaxValue)
                        w = weights[i, c];
                    else
                    {
                        var sum = weights[i, c - it.price] + it.weight;
                        w = Math.Min(weights[i, c], sum);
                    }

                    weights[i + 1, c] = w;
                }
            }

            for (int i = maxPrice; i > 0; i--)
            {
                if (weights[problem.Items.Length, i] <= trimmed.MaxWeight)
                    return Reconstruct(i);
            }
            return null;
        }

        private Result Reconstruct(int n)
        {
            int i = trimmed.Items.Length;
            Result r = new Result();
            r.weight = weights[i, n];
            r.price = 0;
            while (i > 0 && n>0)
            {
                if (weights[i - 1, n] != weights[i, n])
                {
                    r.price += problem.Items[i-1].price;
                    n -= trimmed.Items[i-1].price;
                    i--;         
                }
                else if(weights[i - 1, n] == weights[i, n])
                {
                    i--;
                }
            }
            return r;
        }

        private void Init()
        {
            trimmed = new KnapsackProblem(problem.MaxWeight,(Item[]) problem.Items.Clone());
            int sum = problem.Items.Sum(i => i.price);

            K = (int)Math.Ceiling(epsilon*sum/problem.Items.Length);

            for (int i = 0; i < trimmed.Items.Length; i++)
            {
                trimmed.Items[i].price /= K;
            }

            maxPrice = trimmed.Items.Sum(i => i.price);
            weights = new int[trimmed.Items.Length + 1, maxPrice + 1];

            for (int i = 1; i <= maxPrice; i++)
            {
                weights[0, i] = int.MaxValue;
            }

            weights[0, 0] = 0;
        }

        public static FptasAlgorithm Create(KnapsackProblem problem)
        {
            return new FptasAlgorithm(problem,0.1f);
        }

        private int[,] weights;
        private int maxPrice;
        private int K;
        private float epsilon;
        private KnapsackProblem trimmed;
    }
}
