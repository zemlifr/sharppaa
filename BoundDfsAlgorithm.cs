﻿namespace SharpPAA
{
    class BoundDfsAlgorithm : Algorithm
    {
        public BoundDfsAlgorithm(KnapsackProblem problem) : base(problem)
        {
        }

        public override Result Solve()
        {
            states = 0;
            best = new Result();
            return Solve(new Result(), 0);
        }

        private Result Solve(Result crnt, int it)
        {
            //measurment
            states++;

            if (it >= problem.Items.Length)
                return crnt;

            Result left = new Result(crnt);
            if (left.weight + problem.Items[it].weight < problem.MaxWeight)
            {
                left.price += problem.Items[it].price;
                left.weight += problem.Items[it].weight;
                if (left.price + RestValues(it + 1) > best.price)
                    left = Solve(left, it + 1);
            }

            Result right = new Result(crnt);
            if(right.price+RestValues( it + 1)>best.price)
                right = Solve(right, it + 1);

            Result ret;

            if (problem.isValid(left) && problem.isValid(right))
                ret = left.price > right.price ? left : right;
            else if (problem.isValid(left) && !problem.isValid(right))
                ret = left;
            else
                ret = right;

            if (best.price < ret.price)
                best = ret;

            return ret;
        }

        private int RestValues(int i)
        {
            int sum = 0;
            for (; i < problem.Items.Length; i++)
                sum += problem.Items[i].price;

            return sum;
        }

        public static BoundDfsAlgorithm Create(KnapsackProblem problem)
        {
            return new BoundDfsAlgorithm(problem);
        }

        private Result best;
    }
}

