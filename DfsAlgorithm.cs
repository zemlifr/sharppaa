﻿namespace SharpPAA
{
    class DfsAlgorithm : Algorithm
    {
        public DfsAlgorithm(KnapsackProblem problem) : base(problem)
        {
        }

        public override Result Solve()
        {
            return Solve(new Result(), 0);
        }

        private Result Solve(Result crnt, int it)
        {
            if (it >= problem.Items.Length)
                return crnt;

            Result left = new Result(crnt);
            left.price += problem.Items[it].price;
            left.weight += problem.Items[it].weight;
            left = Solve(left, it + 1);

            Result right = new Result(crnt);
            right = Solve(right, it + 1);

            if (problem.isValid(left) && problem.isValid(right))
                return left.price > right.price ? left : right;
            if (problem.isValid(left) && !problem.isValid(right))
                return left;

            return right;
        }

        public static DfsAlgorithm Create(KnapsackProblem problem)
        {
            return new DfsAlgorithm(problem);
        }
    }
}
