﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpPAA
{
    class Test
    {
        public static void TestTime(KnapsackProblem[] problems, AlgCreator cr)
        {
            var watch = Stopwatch.StartNew();
            for (int i = 0; i < problems.Length * 5; i++)
            {
                var problem = problems[i % 5];
                var alg = cr(problem);
                var res = alg.Solve();
            }
            watch.Stop();
            Console.Out.WriteLine(watch.Elapsed.TotalMilliseconds * 1000000 / (50 * 5));
        }

        public static void TestStates(KnapsackProblem[] problems, AlgCreator cr)
        {
            var states = 0;

            foreach (var problem in problems)
            {
                var alg = cr(problem);
                alg.Solve();
                states += alg.StateCount;
            }
            Console.Out.WriteLine(states/problems.Length);
        }

        public static void TestError(KnapsackProblem[] problems, AlgCreator cr)
        {
            double error = 0;
            double maxError = 0;
            foreach (var problem in problems)
            {
                var alg = cr(problem);
                var alg2 = new DynamicAlgorithm(problem);
                var res = alg.Solve();
                var res2 = alg2.Solve();

                error += (res2.price - (double)res.price) / res2.price;
                if (error > maxError)
                    maxError = error;
            }
            var result = (error/problems.Length);
            Console.Out.WriteLine(result.ToString("##.#####"));
            //Console.Out.WriteLine(maxError.ToString("##.#####"));
        }

        public delegate Algorithm AlgCreator(KnapsackProblem p);
    }
}
