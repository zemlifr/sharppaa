﻿using System;
using System.IO;

namespace SharpPAA
{
    class KnapsackProblem
    {

        public KnapsackProblem(int max, Item [] items)
        {
            maxWeight = max;
            this.items = items;
        }

        public KnapsackProblem()
        {
            maxWeight = 0;
        }

        public bool isValid(Result res)
        {
            return res.weight <= maxWeight;
        }

        public int MaxWeight
        {
            get { return maxWeight; }
            set { maxWeight = value; }
        }

        public Item[] Items => items;

        public static KnapsackProblem FromText(TextReader reader)
        {
            Item[] items;
            
            var line = reader.ReadLine();
            var words = line.Split(' ');
            int id, n, m, we, pr;

            id = int.Parse(words[0]);
            n = int.Parse(words[1]);
            m = int.Parse(words[2]);

            items = new Item[n];

            for (int i = 3, j =0; i < words.Length; i += 2, j++)
            {
                we = int.Parse(words[i]);
                pr = int.Parse(words[i+1]);
                items[j] = new Item(pr,we);
            }
            
            return new KnapsackProblem(m,items);
        }

        private int maxWeight;
        private readonly Item [] items;
    }

    struct Item
    {
        public int price;
        public int weight;
        public Item(int p, int w)
        {
        price = p;
        weight = w;
        }
    };
}
