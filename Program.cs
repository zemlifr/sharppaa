﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Configuration;

namespace SharpPAA
{
    class Program
    {
        private static void Main(string[] args)
        {
            var reader = new StreamReader(new FileStream(args.Last(), FileMode.Open, FileAccess.Read));
            KnapsackProblem[] problems = new KnapsackProblem[50];
            for (int i = 0; i < problems.Length; i++)
                problems[i] = KnapsackProblem.FromText(reader);

            reader.Close();

            //var alg = SelectAlg(args);
            //SharpPAA.Test.TestTime(problems,alg);
            var watch = Stopwatch.StartNew();
            for (int i = 0; i < problems.Length * 5; i++)
            {
                var problem = problems[i % 5];
                var alg = new GeneticAlgorithm(problem);
                var res = alg.Solve();
            }
            watch.Stop();
            Console.Out.WriteLine(watch.Elapsed.TotalMilliseconds * 1000000 / (50 * 5));
        }

        private static Test.AlgCreator SelectAlg(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "bound":
                        return BoundDfsAlgorithm.Create;
                    case "fptas":
                        return FptasAlgorithm.Create;
                    case "dynamic":
                        return DynamicAlgorithm.Create;
                    case "heuristic":
                        return HeuristicAlgorithm.Create;
                }
            }
            return DfsAlgorithm.Create;
        }

        private void Test()
        {
            Item[] items = new Item[5];
            items[0].price = 5;
            items[0].weight = 2;
            items[1].price = 9;
            items[1].weight = 6;
            items[2].price = 20;
            items[2].weight = 5;
            items[3].price = 12;
            items[3].weight = 3;
            items[4].price = 18;
            items[4].weight = 4;
            KnapsackProblem problem = new KnapsackProblem(10, items);
            FptasAlgorithm alg = new FptasAlgorithm(problem, 0.0001f);
            Result res = alg.Solve();
            Console.Out.WriteLine(res.weight);
        }

    }
}
