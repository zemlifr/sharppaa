﻿namespace SharpPAA
{
    abstract class Algorithm 
    {
        protected Algorithm(KnapsackProblem problem)
        {
            this.problem = problem;
        }
        Algorithm() { }

        public abstract Result Solve();

        public int StateCount => states;

        protected KnapsackProblem problem;
        protected int states;
    }

    public class Result
    {
        public int weight;
        public int price;

        public Result(int price, int weight)
        {
            this.weight = weight;
            this.price = price;
        }

        public Result(Result cl)
        {
            weight = cl.weight;
            price = cl.price;
        }

        public Result() { }
    }

}
