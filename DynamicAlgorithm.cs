﻿using System;
using System.Linq;

namespace SharpPAA
{
    class DynamicAlgorithm : Algorithm
    {
        public DynamicAlgorithm(KnapsackProblem problem) : base(problem)
        {
        }

        public override Result Solve()
        {
            states = 0;
            Init();
            Item[] items = new Item[problem.Items.Length + 1];
            items[0] = new Item(0, 0);
            Array.Copy(problem.Items, 0, items, 1, problem.Items.Length);

            for (int i = 0; i < problem.Items.Length; i++)
            {
                for (int c = 0; c <= maxPrice; c++)
                {
                    int w;
                    Item it = items[i + 1];
                    if (c - it.price < 0 || weights[i, c - it.price] == int.MaxValue)
                        w = weights[i, c];
                    else
                    {
                        var sum = weights[i, c - it.price] + it.weight;
                        w = Math.Min(weights[i, c], sum);
                    }

                    weights[i + 1, c] = w;
                    states++;
                }
            }

            for (int i = maxPrice; i > 0; i--)
            {
                if(weights[problem.Items.Length,i]<=problem.MaxWeight)
                    return new Result(i, weights[problem.Items.Length, i]);
            }
            return null;
        }
        

        private void Init()
        {
            maxPrice = problem.Items.Sum(i => i.price);
            weights = new int[problem.Items.Length+1, maxPrice+1];

            for (int i = 1; i <= maxPrice; i++)
            {
                weights[0,i] = int.MaxValue;
            }

            weights[0, 0] = 0;
        }

        public static DynamicAlgorithm Create(KnapsackProblem problem)
        {
            return new DynamicAlgorithm(problem);
        }

        private int[,] weights;
        private int maxPrice;
    }

}
