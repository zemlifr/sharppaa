﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpPAA
{
    class HeuristicAlgorithm : Algorithm
    {
        public HeuristicAlgorithm(KnapsackProblem problem) : base(problem)
        {
        }

        public override Result Solve()
        {
            states = 0;
            var sorted = problem.Items;
            Array.Sort(sorted, new RatioComparator());

            Result res = new Result();

            foreach(var i in sorted)
            {
                states++;

                if (res.weight + i.weight < problem.MaxWeight)
                {
                    res.weight += i.weight;
                    res.price += i.price;
                }
                else break;
            }

            return res;
        }

        public static HeuristicAlgorithm Create(KnapsackProblem problem)
        {
            return new HeuristicAlgorithm(problem);
        }

        private class RatioComparator : IComparer<Item>
        {
            public int Compare(Item x, Item y)
            {
                var a = (double)x.price/ x.weight;
                var b = (double)y.price / y.weight;

                return -a.CompareTo(b);
            }
        }
    }
}
